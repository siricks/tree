<?php
/* Настройки подключения к базе */
define("localhost",     "localhost");
define("db_login",    "root");
define("db_password", "");
define("db_name", "storage");



class Storage
{
    private $mysqli;
    public $parents;
    public $children;

    /**
     * Storage constructor.
     * @param $node_id
     */
    function __construct($node_id)
    {
        $this->parents = $this->searchNode($node_id, 0);
        $this->children = $this->searchNode($node_id, 1);
    }

    /**
     * Функция поиска заданного узла и всех его предков
     * @param $node_id
     * @return array
     */
    private function searchNode($node_id, $param)
    {
        $this->connect();
        if ($param == 0){
            $stmt = $this->mysqli->prepare("SELECT n.name, n.id, n.parent_id FROM node n, reverse_index r WHERE r.node_id=? AND r.found_id=n.id");
        }else{
            $stmt = $this->mysqli->prepare("SELECT n.name, n.id, n.parent_id FROM node n, reverse_index r WHERE r.found_id=? AND r.node_id=n.id");
        }
        $stmt->bind_param('i', $node_id);
        $stmt->execute();
        $result = $stmt->get_result();
        if ( $result->num_rows == 0 ) {
            $node[] = $this->searchOneNode($node_id);
        }else{
            while ($row = $result->fetch_assoc()) {
                $node[] = $row;
            }
        }
        $stmt->close();
        $this->mysqli->close();
        return $node;
    }

    /**
     * Поиск узла, не имеющего предков
     * @param $node_id
     * @return mixed
     */
    private function searchOneNode($node_id)
    {
        $stmt = $this->mysqli->prepare("SELECT name, id, parent_id FROM node WHERE id=?");
        $stmt->bind_param('i', $node_id);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows != 0){
            return $result->fetch_assoc();
        }else{
            return false;
        }
    }

    /**
     * Простейшая функция вывода
     * @param $nodes
     */
    public function viewResult($nodes)
    {
        foreach ($nodes as $node){
            if ($node != false){
                printf("Name: %s, ID: %s, Parent_id: %s <br>", $node['name'], $node['id'], $node['parent_id']);
            }else{
                echo "Node not found";
            }
        }
    }

    /**
     * Создаем соединение с бд
     */
    private function connect()
    {
        $this->mysqli = new mysqli(localhost, db_login, db_password, db_name);
        if (mysqli_connect_errno()) {
            echo "Подключение к серверу MySQL невозможно. Код ошибки: " . mysqli_connect_error();
        }
    }

}
$storage = new Storage(4);
$storage->viewResult($storage->parents);
$storage->viewResult($storage->children);